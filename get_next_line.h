/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 10:43:31 by jrossign          #+#    #+#             */
/*   Updated: 2021/11/04 13:12:58 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include <unistd.h>
# include <stdlib.h>
# include <stddef.h>

char	*get_next_line(int fd);
char	*ft_bufferjoin(char *line, const char *buffer);
char	*ft_strtrim(const char *str);
int		ft_strlen(char *str);
int		ft_checkstr(const char *str);
void	ft_bufftrim(char *buffer, char *buffer_left);
#endif
