# Cursus42 - get_next_line

You can find the subject there : [Subject](https://gitlab.com/jeanbenoit.rossignol.eleve/cursus42-get_next_line/-/blob/norminette/en.subject.pdf)

You can find the norms there : [Norms](https://gitlab.com/jeanbenoit.rossignol.eleve/cursus42-get_next_line/-/blob/norminette/Norminette.pdf)

# Getting started

The c files can be compiled using gcc, a main function will be needed to compile it.

### Prerequisites

- Install gcc\
Using brew :
`brew install gcc`\
Linux os :
`packagemanager gcc`

### Using the program with main

Using the command `gcc your_main.c *.c get_next_line.h -D BUFFER_SIZE=x -o program_name` where x is the\
amount of characters you want to read, will create the program named program_name.\

- o flag : write the build output to an output file.

- D flag : defines a macro to be used by the preprocessor.

# Tester

To test the program, I used a tester made by someone from an other 42 campus. [Sources](#4--Sources)

# Source 

### Flags
1. [o flag](https://www.rapidtables.com/code/linux/gcc/gcc-o.html#output%20file)
2. [D flag](https://www.rapidtables.com/code/linux/gcc/gcc-d.html)

### Tester
3. [Tripouille](https://github.com/Tripouille/gnlTester)
