/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 15:24:07 by jrossign          #+#    #+#             */
/*   Updated: 2021/11/04 14:38:08 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

char	*get_next_line(int fd)
{
	static char	buffer_left[BUFFER_SIZE + 1];
	char		buffer[BUFFER_SIZE + 1];
	char		*line;
	int			i;

	line = NULL;
	buffer[0] = '\0';
	if (buffer_left[0] != '\0')
	{
		line = ft_bufferjoin(line, buffer_left);
		ft_bufftrim(buffer_left, buffer_left);
	}
	while (ft_checkstr(line))
	{
		i = read(fd, buffer, BUFFER_SIZE);
		buffer[i] = '\0';
		if (ft_strlen(line) > 0 && i == 0)
			break ;
		if (i == 0 || i == -1)
			return (NULL);
		line = ft_bufferjoin(line, buffer);
	}
	if (buffer_left[0] == '\0')
		ft_bufftrim(buffer, buffer_left);
	return (ft_strtrim(line));
}
