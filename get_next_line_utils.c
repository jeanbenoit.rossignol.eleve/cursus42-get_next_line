/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jrossign <jrossign@student.42quebec.com    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/02 12:17:54 by jrossign          #+#    #+#             */
/*   Updated: 2021/11/04 14:24:50 by jrossign         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

void	ft_bufftrim(char *buffer, char *buffer_left)
{
	int	i;
	int	j;

	i = 0;
	j = 0;
	while (buffer[i] && buffer[i] != '\n')
		i++;
	if (buffer[i] == '\n')
		i++;
	while (buffer[i])
	{
		buffer_left[j++] = buffer[i++];
	}
	while (buffer_left[j])
		buffer_left[j++] = '\0';
}

char	*ft_bufferjoin(char *line, const char *buffer)
{
	int		i;
	int		linelen;
	char	*newline;

	linelen = ft_strlen(line);
	newline = malloc((linelen + ft_strlen((char *)buffer) + 1) * sizeof(char));
	if (!newline)
		return (NULL);
	i = 0;
	while (i < linelen && line)
	{
		newline[i] = line[i];
		i++;
	}
	i = 0;
	while (buffer && buffer[i])
	{
		newline[i + linelen] = buffer[i];
		i++;
	}
	newline[i + linelen] = '\0';
	free(line);
	return (newline);
}

char	*ft_strtrim(const char *str)
{
	int		i;
	char	*finalstr;

	i = 0;
	if (!str)
		return (NULL);
	while (str[i] && str[i] != '\n')
		i++;
	if (str[i] == '\n')
		i++;
	finalstr = malloc(i + 1 * sizeof(char));
	if (!finalstr)
		return (NULL);
	i = 0;
	while (str[i] && str[i] != '\n')
	{
		finalstr[i] = str[i];
		i++;
	}
	if (str[i] == '\n')
		finalstr[i++] = '\n';
	finalstr[i] = '\0';
	free((char *)str);
	return (finalstr);
}

int	ft_strlen(char *str)
{
	int	i;

	if (!str)
		return (0);
	i = 0;
	while (str[i])
		i++;
	return (i);
}

int	ft_checkstr(const char *str)
{
	int	i;

	if (!str)
		return (1);
	i = 0;
	while (str[i])
	{
		if (str[i] == '\n')
			return (0);
		i++;
	}
	return (1);
}
